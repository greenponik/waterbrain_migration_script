#!/bin/sh
# migrate.sh
# ------------------------------------------------------------
# Object: This script allow GreenPonik SAS to manage migration
# of current waterbrain version to the next stable version.
# Distribution: Raspbian Buster
# Works on:
#           -Raspberry Pi Zero W
#           -Raspberry Pi 3 B+
#           -Raspberry Pi 3 A+
# Licence: no licence private script copyright GreenPonik SAS
# Author: Mickael Lehoux <mickael.lehoux@greenponik.com>
# Details:
# -- actions don't need  reboot
#   - enable ssh i2c and gpio
#   - add greenponik user
#   - allow greenponik user to use ssh
#   - if nginx or apache service is existing stop and disable it
#   - install dependencies
#   - clone waterbrain repository
# -- actions need reboot
#   - define hostname from mac address
#   - enable multiple i2c busses on /boot/config.txt
#   - limit gpu memory allocation on /boot/config.txt
#   - install docker and allow greenponik user to use it
#   - apply good rights on waterbrain folder
# ------------------------------------------------------------

# set -x
VERSION="2021-11-26 v0.0.3"
DEFAULT='\033[0;39m'
WHITE='\033[0;02m'
RASPBERRY='\033[0;35m'
GREEN='\033[1;32m'
ORANGE='\033[1;33m'
RED='\033[1;31m'
BLUE='\033[1;34m'
CYAN='\033[1;36m'

_usage() {
    cat 1>&2 <<EOF
Update the WaterBrain to the latest version

USAGE:
    # without any installation
    wget -O- "https://gitlab.com/greenponik/waterbrain_migration_script/-/raw/main/migrate.sh" | bash

    # on local mode
    bash migrate.sh

FLAGS:
    -sgp, --skip-git-pull      	skip the git pull process
    -h, --help                  Show this help
EOF
    exit 0
}

POSITIONAL=()
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
    -sgp | --skip-git-pull)
        SKIP_GIT_PULL="true"
        shift
        ;;
    -h | --help)
        _usage
        shift
        ;;
    *)
        POSITIONAL+=("$1")
        shift
        ;;
    esac
done
set -- "${POSITIONAL[@]}"

_welcome() {
    echo -e "${RASPBERRY}\n"
    echo -e "                                                                                                  "
    echo -e "     ----------------------------------------------------------------------------------------     "
    echo -e "                                                                                                  "
    echo -e "  /88      /88             /88                         /8888888                     /88           "
    echo -e " | 88  /8 | 88            | 88                        | 88__  88                   |__/           "
    echo -e " | 88 /888| 88  /888888  /888888    /888888   /888888 | 88  \ 88  /888888  /888888  /88 /8888888  "
    echo -e " | 88/88 88 88 |____  88|_  88_/   /88__  88 /88__  88| 8888888  /88__  88|____  88| 88| 88__  88 "
    echo -e " | 8888_  8888  /8888888  | 88    | 88888888| 88  \__/| 88__  88| 88  \__/ /8888888| 88| 88  \ 88 "
    echo -e " | 888/ \  888 /88__  88  | 88 /88| 88_____/| 88      | 88  \ 88| 88      /88__  88| 88| 88  | 88 "
    echo -e " | 88/   \  88|  8888888  |  8888/|  8888888| 88      | 8888888/| 88     |  8888888| 88| 88  | 88 "
    echo -e " |__/     \__/ \_______/   \___/   \_______/|__/      |_______/ |__/      \_______/|__/|__/  |__/ "
    echo -e "                                                                                                  "
    echo -e "     ----------------------------------------------------------------------------------------     "
    echo -e "                                                                                                  "
    echo -e "                /88                                /88     /88                                    "
    echo -e "               |__/                               | 88    |__/                                    "
    echo -e "  /888888/8888  /88  /888888   /888888  /888888  /888888   /88  /888888  /8888888                 "
    echo -e " | 88_  88_  88| 88 /88__  88 /88__  88|____  88|_  88_/  | 88 /88__  88| 88__  88                "
    echo -e " | 88 \ 88 \ 88| 88| 88  \ 88| 88  \__/ /8888888  | 88    | 88| 88  \ 88| 88  \ 88                "
    echo -e " | 88 | 88 | 88| 88| 88  | 88| 88      /88__  88  | 88 /88| 88| 88  | 88| 88  | 88                "
    echo -e " | 88 | 88 | 88| 88|  8888888| 88     |  8888888  |  8888/| 88|  888888/| 88  | 88                "
    echo -e " |__/ |__/ |__/|__/ \____  88|__/      \_______/   \___/  |__/ \______/ |__/  |__/                "
    echo -e "                    /88  \ 88                                                                     "
    echo -e "                   |  888888/                                                                     "
    echo -e "                    \______/                                                                      "
    echo -e "                                                                                                  "
    echo -e "                                                                                                  "
    echo -e " By https://greenponik.com - version: ${VERSION}                                                  "
    echo -e "${DEFAULT}                                                                                        "
    echo -e "Migration of WaterBrain - (CTRL+C to exit | --help to see options)\n\n                            "
}

_logger() {
    echo -e "${1}"
    echo "${2}"
    echo -e "${DEFAULT}"
}
_write_step() {
    echo "step=$1" >${CONF_INI}
}

_read_step() {
    echo $(grep "step=" ${CONF_INI} | cut -d "=" -f2)
}

if ! test -v SKIP_GIT_PULL; then
    SKIP_GIT_PULL="false"
fi

TOTAL_STEPS=(0 1 2 3 4 5 6 7)
CONF_INI=$(pwd)/migrate.ini
AP_STA_PATH=/var/lib/waterbrain/AP_STA_RPI_SAME_WIFI_CHIP/
SQLITE_PATH=/var/lib/waterbrain/data/
SSL_PATH=/var/lib/waterbrain/ssl/
EC_BUFFER_PATH=/var/lib/waterbrain/data/
PH_BUFFER_PATH=/var/lib/waterbrain/data/
CURRENT_PH_TYPE_DOSING_PATH=/var/lib/waterbrain/data/
WATERTEMP_READS_BUFFER=/var/lib/waterbrain/data/
RPI_CONFIG_PATH=/var/lib/waterbrain/install/
LOGS_PATH=/var/lib/waterbrain/logs/
BKP_PATH=/var/lib/waterbrain/backups/

# welcome cli user
_welcome

# create ini file with last step to restart to the next level when re-run script
if ! [[ -d ${CONF_INI} ]] || [[ ${#TOTAL_STEPS[@]} == $(_read_step) ]]; then
    _write_step 0
fi

if test true == ${SKIP_GIT_PULL}; then
    _logger ${RED} "WARNING you'll skip the git pull and it can destroy the whole customer device and waste our time!"
    _logger ${RED} "so be sure you know what's happend when you confirm the skip!"

    read -p "please confirm skipping the git pull? (y/n)" -n 1 -r
    echo

    if [[ $REPLY =~ ^[Yy]$ ]]; then
        _logger ${ORANGE} "ok! let's go!"
        _write_step 1
    else
        _logger ${ORANGE} "please refere to IT team to fix the current issue! The script will stop"
        _write_step 0
        exit 1
    fi
fi

if [[ 0 == $(_read_step) ]] && test true != ${SKIP_GIT_PULL}; then
    _logger ${GREEN} "git pull the latest version"
    cd /home/greenponik/waterbrain/ &&
        git pull || { _logger ${RED} "no git pull available because script have already run on time"; exit 1; }
    cd /home/greenponik
    _write_step 1
fi

if [[ 1 == $(_read_step) ]]; then
    _logger ${GREEN} "stop every containers"
    cd /home/greenponik/waterbrain/ &&
        docker-compose stop core scheduler nginx watchtower
    cd /home/greenponik
    _write_step 2
fi

if [[ 2 == $(_read_step) ]]; then
    _logger ${GREEN} "replace the current docker-compose.yml and pull the new waterbrain docker images"
    cd /home/greenponik/waterbrain
    if test -f docker-compose.prod.yml; then
        mv docker-compose.yml _old_docker-compose.yml
        mv docker-compose.prod.yml docker-compose.yml
    fi
    cd /home/greenponik
    _write_step 3
fi

if [[ 3 == $(_read_step) ]]; then
    _logger ${GREEN} "change the crontab rpi-config path"
    cd /home/greenponik/waterbrain
    old_crontask="/home/greenponik/waterbrain/install/rpi-config.sh"
    new_crontask="/var/lib/waterbrain/install/rpi-config.sh"

    if [[ 1 -eq $(sudo crontab -l | grep -cF "${old_crontask}") ]]; then
        _logger ${GREEN} "replace old crontask by new"
        sudo crontab -l >cron_jobs
        sed -i "s|${old_crontask}|${new_crontask}|g" cron_jobs
        sudo crontab cron_jobs
    fi

    if [[ 1 -ne $(sudo crontab -l | grep -cF "${new_crontask}") ]]; then
        _logger ${RED} "there an error on crontab please check it now! before retry to run th script."
        _write_step 0
        exit 1
    else
        _logger ${GREEN} "new crontask added or already ok"
        if test -f cron_jobs; then
            rm cron_jobs
        fi
    fi
    cd /home/greenponik
    _write_step 4
fi

if [[ 4 == $(_read_step) ]]; then
    _logger ${GREEN} "move files from old paths to new paths"

    cd /home/greenponik/waterbrain/install && 
        sudo mkdir -p ${RPI_CONFIG_PATH} &&
        sudo mv rpi-config.template.sh ${RPI_CONFIG_PATH}rpi-config.sh

    cd /home/greenponik/AP_STA_RPI_SAME_WIFI_CHIP &&
        sudo mkdir -p ${AP_STA_PATH} &&
        sudo mv *.sh ${AP_STA_PATH}

    cd /home/greenponik/waterbrain/django_app &&
        sudo mkdir -p ${SQLITE_PATH} &&
        sudo mv db.sqlite3 ${SQLITE_PATH} &&
        
        sudo mkdir -p ${CURRENT_PH_TYPE_DOSING_PATH} &&
        sudo mv current_ph_type_dosing ${CURRENT_PH_TYPE_DOSING_PATH} &&
        
        sudo mkdir -p ${EC_BUFFER_PATH} &&
        sudo mv ec_reads_buffer ${EC_BUFFER_PATH} &&
        
        sudo mkdir -p ${PH_BUFFER_PATH} &&
        sudo mv ph_reads_buffer ${PH_BUFFER_PATH} &&
        
        sudo mkdir -p ${PH_BUFFER_PATH} &&
        sudo mv watertemp_reads_buffer ${PH_BUFFER_PATH}

    cd /home/greenponik/waterbrain/django_app/logs &&
        sudo mkdir -p ${LOGS_PATH} &&
        sudo mv *.log *.log.* *.log.*.zip ${LOGS_PATH}

    cd /home/greenponik/waterbrain/django_app/backups &&
        sudo mkdir -p ${BKP_PATH} &&
        sudo mv *.zip ${BKP_PATH}

    cd /home/greenponik/waterbrain/django_app/ssl &&
        sudo mkdir -p ${SSL_PATH} &&
        sudo mv *.crt *.key ${SSL_PATH}

    cd /home/greenponik
    _write_step 5
fi

if [[ 5 == $(_read_step) ]]; then
    _logger ${GREEN} "cleanup the waterbrain/ folder"
    declare -A CHECK_STATUS
    CHECK_STATUS[db]=1
    CHECK_STATUS[config_script]=2
    CHECK_STATUS[ph_type_buff]=3
    CHECK_STATUS[ec_buff]=4
    CHECK_STATUS[ph_buff]=5
    CHECK_STATUS[water_buff]=6
    CHECK_STATUS[all_ok]=21

    flag_all_is_ok=0
    if [[ -f "${SQLITE_PATH}/db.sqlite3" ]]; then
        ((flag_all_is_ok = flag_all_is_ok + ${CHECK_STATUS[db]}))
    fi
    if [[ -f "${RPI_CONFIG_PATH}/rpi-config.sh" ]]; then
        ((flag_all_is_ok = flag_all_is_ok + ${CHECK_STATUS[config_script]}))
    fi
    if [[ -f "${SQLITE_PATH}/current_ph_type_dosing" ]]; then
        ((flag_all_is_ok = flag_all_is_ok + ${CHECK_STATUS[ph_type_buff]}))
    fi
    if [[ -f "${SQLITE_PATH}/ec_reads_buffer" ]]; then
        ((flag_all_is_ok = flag_all_is_ok + ${CHECK_STATUS[ec_buff]}))
    fi
    if [[ -f "${SQLITE_PATH}/ph_reads_buffer" ]]; then
        ((flag_all_is_ok = flag_all_is_ok + ${CHECK_STATUS[ph_buff]}))
    fi
    if [[ -f "${SQLITE_PATH}/watertemp_reads_buffer" ]]; then
        ((flag_all_is_ok = flag_all_is_ok + ${CHECK_STATUS[water_buff]}))
    fi
    if [[ $flag_all_is_ok -ne CHECK_STATUS[all_ok] ]]; then
        _logger ${RED} " cannot process the cleanup because a file is missing ${flag_all_is_ok}"
        _write_step 0
        exit 1
    else
        cd /home/greenponik &&
            rm -rf /home/greenponik/AP_STA_RPI_SAME_WIFI_CHIP
        cd /home/greenponik/waterbrain &&
            rm -rf AP_STA_RPI_SAME_WIFI_CHIP &&
            rm -rf .git &&
            rm -rf django_app &&
            rm -rf docker &&
            rm -rf install &&
            rm -rf pwa_app &&
            rm -rf readme.md &&
            rm -rf .gitattributes &&
            rm -rf .gitignore &&
            rm -rf .dockerignore &&
            rm -rf .gitlab-ci.yml &&
            rm -rf pipelines_cleaner.sh &&
            rm -rf docker-compose.dev.yml &&
            rm -rf docker-compose.staging.yml
            rm -rf docker-compose.demo.yml
        cd /home/greenponik
    fi
    _write_step 6
fi

if [[ 6 == $(_read_step) ]]; then
    _logger ${GREEN} "restart new containers"
    cd /home/greenponik/waterbrain &&
        docker-compose pull core scheduler server gui &&
        docker-compose up -d --remove-orphans --force-recreate core scheduler server gui  &&
        docker image prune -f
    cd /home/greenponik
    _write_step 7
fi

if [[ 7 == $(_read_step) ]]; then
    _logger ${GREEN} "the waterbrain will reboot in 10 sec"
    wget -O- https://raw.githubusercontent.com/azlux/log2ram/master/uninstall.sh | sudo bash
    sleep 10
    _write_step 8
    sudo reboot
fi

# if [[ 2 == $(_read_step) ]]; then
#     _logger ${GREEN} "stop autodosing"
#     docker-compose exec core python manage.py stop_autodosing
#     _logger ${GREEN} "put every pumps state to false"
#     docker-compose exec core python manage.py stop_all_pumps
#     _write_step 3
# fi