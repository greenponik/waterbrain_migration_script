# waterbrain_migration_script

2 cases:

- 1 The first time your run the script

```shell
wget -O- "https://gitlab.com/greenponik/waterbrain_migration_script/-/raw/main/migrate.sh" | bash
```

- 2 The case you need to run the script but the git repository is already deleted
skipping the mandatory git pull
WARNING this method is only for user knows what they are doing!
to allow interactions using this syntax

```shell
bash <(wget -qO- "https://gitlab.com/greenponik/waterbrain_migration_script/-/raw/main/migrate.sh") --skip-git-pull
```

## WARNING

**take care about one thing never stop or restart the “remote” container because the device will unreachable remotely.**

## 1 step

Before to start any script, you need to pull the current master branch from gitlab on /home/greenponik/waterbrain/

```shell
/home/greenponik/waterbrain: git pull
```

user and password are yours.

## 2 step

When git pull is finished, restart "core" container because it's create new needed files on the devices.

```shell
/home/greenponik/waterbrain: docker-compose restart core
```

When these steps are finished run the script:

```shell
wget -O- "https://gitlab.com/greenponik/waterbrain_migration_script/-/raw/main/migrate.sh" | bash
```

**During the script execution please keep putty online by scrolling everytime the windows becasue putty or ngrok will close the current session due to unactivity and broke the migration process.**

## 3 step

Check the essential thing: "crontab" because if the crontab don't exist the initialisation script never run and the device never works.

```shell
sudo crontab -e
```

Have to return:

```shell
# run rpi-config.sh on each boot
@reboot sleep 30 && sudo /bin/bash /var/lib/waterbrain/install/rpi-config.sh >> /var/log/rpi-config.log 2>&1

# comment for crontab init

# Start hostapd when ap0 already exists
* * * * * /bin/bash /bin/manage-ap0-iface.sh >> /var/log/ap_sta_wifi/ap0_mgnt.log 2>&1

# On boot start AP + STA config
@reboot sleep 20 && /bin/bash /bin/rpi-wifi.sh >> /var/log/ap_sta_wifi/on_boot.log 2>&1
```

**Take care sometimes the script intiate double rpi-config.sh command**

## 4 step

Check the /home/greenponik/waterbrain/docker-compose.yml content because if the wget action is stopped during the dowload the docker-compose.yml is empty.

```shell
cd /home/greenponik/waterbrain
nano docker-compose.yml
```

will return:

```shell
version: "3.8"

services:
  watchtower:
    image: containrrr/watchtower
    restart: always
    network_mode: "host"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    environment:
      - WATCHTOWER_CLEANUP=true
      - WATCHTOWER_DEBUG=false
      - WATCHTOWER_TIMEOUT=30s
      - WATCHTOWER_POLL_INTERVAL=1800
      - WATCHTOWER_INCLUDE_RESTARTING=true
      - WATCHTOWER_ROLLING_RESTART=true
      - TZ="Europe/Paris"
    logging:
      driver: "json-file"
      options:
        max-size: "200k"
        max-file: "5"
  core:
    image: greenponik/waterbrain-core:prod
    restart: always
    privileged: true
    network_mode: "host"
    volumes:
      - /var/lib/waterbrain/:/var/lib/waterbrain/
      - /var/run/wpa_supplicant:/var/run/wpa_supplicant
      - /etc/wpa_supplicant/wpa_supplicant.conf:/etc/wpa_supplicant/wpa_supplicant.conf
      - /etc/network/interfaces:/etc/network/interfaces
    expose:
      - "8000"
    # do not use deploy limitations
    # that can break the container communication
    # and give unstable behavior
    # deploy:
    #   resources:
    #     limits:
    #       cpus: '1.5'
    #       memory: 300M
    logging:
      driver: "json-file"
      options:
        max-size: "400k"
/../
```

## 5 step

Check the folder structure in /var/lib/waterbrain/.

if the buffers files miss the migration script will return a code error.

```shell
CHECK_STATUS[db]=1
CHECK_STATUS[config_script]=2
CHECK_STATUS[ph_type_buff]=3
CHECK_STATUS[ec_buff]=4
CHECK_STATUS[ph_buff]=5
CHECK_STATUS[water_buff]=6
CHECK_STATUS[all_ok]=21
```

Status code is the result of the addition of each codes above.
e.g. If the script return and error with the code 6 the migration stopped during the process of the ec_buff.

```shell
ls -la /var/lib/waterbrain:
drwxr-xr-x  8 root       root       4096 Mar 17 10:19 .
drwxr-xr-x 31 root       root       4096 Mar 10 09:07 ..
drwxr-xr-x  2 greenponik greenponik 4096 Mar 17 10:20 AP_STA_RPI_SAME_WIFI_CHIP
drwxr-xr-x  2 root       root       4096 Mar 10 09:09 backups
drwxr-xr-x  2 root       root       4096 Mar 25 10:42 data
drwxr-xr-x  2 root       root       4096 Mar 25 11:29 install
drwxr-xr-x  2 root       root       4096 Mar 11 16:56 logs
drwxr-xr-x  2 root       root       4096 Mar 11 17:15 ssl

```

in the /var/lib/waterbrain/install folder you should find:

 ```shell
 -rw-r--r-- 1 root root     8 Mar 24 17:59 rpi-config.ini
-rw-rw-rw- 1 root root 18853 Mar 25 11:29 rpi-config.sh
 ```

in the /var/lib/waterbrain/data folder you should find:

```shell
drwxr-xr-x 2 root root    4096 Mar 25 10:42 .
drwxr-xr-x 8 root root    4096 Mar 17 10:19 ..
-rw-rw-rw- 1 root root       5 Mar 25 11:29 current_ph_type_dosing
-rw-r--r-- 1 root root 1007616 Mar 25 10:42 db.sqlite3
-rw-rw-rw- 1 root root      53 Mar 25 11:33 ec_reads_buffer
-rw-rw-rw- 1 root root      53 Mar 25 11:33 ph_reads_buffer
-rw-rw-rw- 1 root root      53 Mar 25 11:33 watertemp_reads_buffer
```

in the /var/lib/waterbrain/ssl folder you should find:

```shell
-rw-r--r-- 1 root root  985 Mar 23 19:29 server.crt
-rw------- 1 root root 1704 Mar 23 19:29 server.key
```

## 6 step

Check if some images can be purge on the WaterBrain.

```shell
/home/greenponik/waterbrain: docker images
```

you should find:

```shell
greenponik/waterbrain-remote      prod      a7c2644c1833   5 days ago     507MB
greenponik/waterbrain-scheduler   prod      a11180b47621   5 days ago     507MB
greenponik/waterbrain-server      prod      7d5fcfb237f5   5 days ago     16.8MB
greenponik/waterbrain-core        prod      2ba96c27043d   5 days ago     507MB
greenponik/waterbrain-gui         prod      1b840c9aca5f   7 days ago     449MB
containrrr/watchtower             latest    72324c978d08   2 months ago   14.5MB
```

To free space on device other images can be remove using: docker rmi <image_id>

## 7 step

Check the docker containers are running

```shell
/home/greenponik/waterbrain: docker ps
```

you should find:

```shell
39d690cb40d1   greenponik/waterbrain-scheduler:prod   "python manage.py st…"   3 minutes ago   Up About a minute             waterbrain_scheduler_1
0d79cdefa358   greenponik/waterbrain-remote:prod      "bash -c '(python ma…"   3 minutes ago   Up 40 seconds                 waterbrain_remote_1
77060708c48d   greenponik/waterbrain-gui:prod         "docker-entrypoint.s…"   3 minutes ago   Up 2 minutes                  waterbrain_gui_1
740097dfa40e   greenponik/waterbrain-core:prod        "bash bin/core-start…"   4 minutes ago   Up 3 minutes                  waterbrain_core_1
7277bef6288c   greenponik/waterbrain-server:prod      "/docker-entrypoint.…"   3 days ago      Up 3 days                     waterbrain_server_1
0b96395112af   containrrr/watchtower                  "/watchtower"            3 days ago      Up 3 days                     waterbrain_watchtower_1

```
